package com.example.ventas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Adaptador_LV extends ArrayAdapter<String> {

    private Activity context;
    private String[] opciones;
    private Integer[] icons;


    public Adaptador_LV(Activity context, String[] opciones, Integer[] icons) {
        super(context, R.layout.listview_menu, opciones);

        this.context = context;
        this.icons = icons;
        this.opciones = opciones;


    }


    public View getView(int position, View convertView, ViewGroup parent){
        View r=convertView;
        ViewHolder viewHolder=null;
        if (r==null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.listview_menu, null, true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) r.getTag();

        }
            viewHolder.ivw.setImageResource(icons[position]);
            viewHolder.tvw1.setText(opciones[position]);

            return r;
        }

        class ViewHolder{
            TextView tvw1;
            ImageView ivw;
            ViewHolder(View v){
                tvw1  =(TextView) v.findViewById(R.id.tvopciones);

                ivw   =(ImageView) v.findViewById(R.id.imageview);




            }


        }


    }











