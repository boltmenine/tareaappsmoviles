package com.example.ventas;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MenuPrincipal extends AppCompatActivity {




    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.titulo, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        TextView textViewTitle = (TextView) viewActionBar.findViewById(R.id.action_bar_textview);
        textViewTitle.setText("Evaluacion III");
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        listView=(ListView)findViewById(R.id.listview);

        final String[] opciones = new String[]{"ABCDin","Samsung","PCFactory","Falabella","Paris"};
        final Integer[] icons =  new Integer[]{R.drawable.iconabcdin,R.drawable.iconsamsung,R.drawable.iconpc,R.drawable.iconfala,R.drawable.iconparis};

     Adaptador_LV adaptador = new Adaptador_LV(MenuPrincipal.this,opciones,icons);
     listView.setAdapter(adaptador);

     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
             if(position==0){
                 //Uri uri = Uri.parse("https://www.abcdin.cl/?utm_medium=afiliado&utm_source=mediasur&utm_campaign=prueba_soicos");
                 //startActivity(new Intent(Intent.ACTION_VIEW,uri));
                 Intent intent = new Intent( MenuPrincipal.this, webview.class);
                 startActivity(intent);
             }
             if(position==1){
                 Uri uri = Uri.parse("https://www.samsung.com/cl/?cid=cl_paid_ppc_google-search_corporate_always-on_aon-search_local-content_marca-marca-site-traffic_mass-other&dclid=&gclid=Cj0KCQiAzZL-BRDnARIsAPCJs723hhzd0yqwEMFCtHFRht3mbgB-ZHGHjXwrujglhydPcFQDrB59dW0aAsQrEALw_wcB");
                 startActivity(new Intent(Intent.ACTION_VIEW,uri));

             }
             if(position==2){
                 Uri uri = Uri.parse("https://www.pcfactory.cl/notebooks?categoria=735&papa=636&tipo=Gamer");
                 startActivity(new Intent(Intent.ACTION_VIEW,uri));

             }
             if(position==3){
                 Uri uri = Uri.parse("https://www.falabella.com/falabella-cl/category/cat40052/Computadores?isPLP=1");
                 startActivity(new Intent(Intent.ACTION_VIEW,uri));

             }
             if(position==4){
                 Uri uri = Uri.parse("https://www.paris.cl/tecnologia/computadores/pc-gamer/");
                 startActivity(new Intent(Intent.ACTION_VIEW,uri));

             }



         }
     });



    }
}